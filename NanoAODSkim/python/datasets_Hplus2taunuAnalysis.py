import sys,os
JSONPATH = os.path.realpath(os.path.join(os.getcwd(),"../data"))

# The name of this file MUST contain the analysis name: datasets_<name>Analysis.py.
# E.g. NanoAOD_Hplus2taunuAnalysisSkim.py -> datasets_Hplus2taunuAnalysis.py

class Dataset:
    def __init__(self, url, dbs="global", dataVersion="94Xmc", lumiMask="", name=""):
        self.URL = url
        self.DBS = dbs
        self.dataVersion = dataVersion
        self.lumiMask = lumiMask
        self.name = name

    def isData(self):
        if "Run201" in self.URL:
            return True
        return False

    def getName(self):
        return self.name

    def getYear(self):
        year_re = re.compile("/Run(?P<year>201\d)\S")
        match = year_re.search(self.URL)
        if match:
            return match.group("year")
        else:
            return None

datasets = {}
datasets['2016UL'] = []
datasets['2016UL'].append(Dataset('/Tau/Run2016G-UL2016_MiniAODv2_NanoAODv9-v1/NANOAOD', dataVersion="2016_106XUL", lumiMask=os.path.join(JSONPATH,"Cert_278820-280385_13TeV_Legacy2016_Collisions16_JSON_Run2016G.txt")))


datasets['2017UL'] = []
datasets['2017UL'].append(Dataset('/Tau/Run2017B-UL2017_MiniAODv2_NanoAODv9-v1/NANOAOD', dataVersion="2017_106XUL", lumiMask=os.path.join(JSONPATH,"Cert_297050-299329_13TeV_UL2017_Collisions17_GoldenJSON_Run2017B.txt")))
datasets['2017UL'].append(Dataset('/TTJets_TuneCP5_13TeV-amcatnloFXFX-pythia8/RunIISummer20UL17NanoAODv9-106X_mc2017_realistic_v9-v1/NANOAODSIM'))

datasets["2018UL"] = []


def getDatasets(dataVersion):
    if not dataVersion in datasets.keys():
        print("Unknown dataVersion",dataVersion,", dataVersions available",datasets.keys())
        sys.exit()
    return datasets[dataVersion]
