import awkward as ak
import numpy
import sys
import os
import re

from  METCleaning import METCleaning
from JetCorrections import JEC

def triggerSelection(events,year,leptonflavor):
    if leptonflavor == 11:
        return (events.HLT.Ele23_Ele12_CaloIdL_TrackIdL_IsoVL &
                events.HLT.Ele23_Ele12_CaloIdL_TrackIdL_IsoVL_DZ
        )
    if leptonflavor == 13:
        return events.HLT.Mu17_TrkIsoVVL_Mu8_TrkIsoVVL_DZ
    return None

def lumimask(events,lmask):
    return (lmask.passed(events))

from  METCleaning import METCleaning

def leptonSelection(events, LEPTONFLAVOR):
    if LEPTONFLAVOR == 13:
        return (
            (ak.num(events.Muon) >= 2)
            & (ak.num(events.Muon) <= 3)
            & (ak.sum(events.Muon.charge, axis=1) <= 1)
            & (ak.sum(events.Muon.charge, axis=1) >= -1)
        )

    if LEPTONFLAVOR == 11:
        return (
            (ak.num(events.Electron) == 2)
            & (ak.num(events.Electron) <= 3)
            & (ak.sum(events.Electron.charge, axis=1) <= 1)
            & (ak.sum(events.Electron.charge, axis=1) >= -1)
        )
    return None

def Zboson(events,LEPTONFLAVOR):
    if LEPTONFLAVOR == 13:
        leptonsPlus = events.Muon[(events.Muon.charge == 1)]
        leptonsMinus= events.Muon[(events.Muon.charge == -1)]
    if LEPTONFLAVOR == 11:
        leptonsPlus = events.Electron[(events.Electron.charge == 1)]
        leptonsMinus= events.Electron[(events.Electron.charge == -1)]
    return leptonsPlus[:, 0] + leptonsMinus[:, 0]

def jets(events):
    jet_cands = events.Jet

    leptons = ak.with_name(ak.concatenate([events.Muon, events.Electron], axis=1), 'PtEtaPhiMCandidate')
    jet_cands = drClean(jet_cands,leptons)
#    jet_cands = drClean(jet_cands,events.Muon)
    #jet_cands = smear_jets(events,jet_cands)
#    jet_cands = JEC(events,jet_cands)
    return jet_cands

def smear_jets(events,jets):
    print("check smear1",jets.pt)
    print("check smear2",jets.pt)
    return jets

def drClean(coll1,coll2,cone=0.4):
    from coffea.nanoevents.methods import vector
    j_eta = coll1.eta
    j_phi = coll1.phi
    l_eta = coll2.eta
    l_phi = coll2.phi

    j_eta, l_eta = ak.unzip(ak.cartesian([j_eta, l_eta], nested=True))
    j_phi, l_phi = ak.unzip(ak.cartesian([j_phi, l_phi], nested=True))
    delta_eta = j_eta - l_eta
    delta_phi = vector._deltaphi_kernel(j_phi,l_phi)
    dr = numpy.hypot(delta_eta, delta_phi)
    jets_noleptons = coll1[~ak.any(dr < cone, axis=2)]
    return jets_noleptons

def recalculateMET(jets):
    return 0.

def plotResponce(events,jets,Zboson,out,eweight):
    """
    leadingJet = jets[:,0]
    R_pT = leadingJet.pt/Zboson.pt
    
    MET = recalculateMET(jets)

    METuncl = -MET - jets_all.pt - Zboson.pt
    R_MPF = 1 + MET.pt*(cos(MET.phi)*Zboson.Px() + sin(MET.Phi())*Zboson.Py())/(Zboson.Pt()*Zboson.Pt());
    R_MPFjet1 = -leadingJet.Pt()*(cos(leadingJet.Phi())*Zboson.Px() + sin(leadingJet.Phi())*Zboson.Py())/(Zboson.Pt()*Zboson.Pt());
    R_MPFjetn = -jets_notLeadingJet.Pt()*(cos(jets_notLeadingJet.Phi())*Zboson.Px() + sin(jets_notLeadingJet.Phi())*Zboson.Py())/(Zboson.Pt()*Zboson.Pt());
    R_MPFuncl = -METuncl.Pt()*(cos(METuncl.Phi())*Zboson.Px() + sin(METuncl.Phi())*Zboson.Py())/(Zboson.Pt()*Zboson.Pt());

              hprof2D_ZpT_Mu_RpT[i]->Fill(Zboson.Pt(),mu,R_pT,eweight);
          hprof2D_ZpT_Mu_RMPF[i]->Fill(Zboson.Pt(),mu,R_MPFPF,eweight);
          hprof2D_ZpT_Mu_RMPFjet1[i]->Fill(Zboson.Pt(),mu,R_MPFjet1,eweight);
          hprof2D_ZpT_Mu_RMPFjetn[i]->Fill(Zboson.Pt(),mu,R_MPFjetn,eweight);
          hprof2D_ZpT_Mu_RMPFuncl[i]->Fill(Zboson.Pt(),mu,R_MPFuncl,eweight);
    """

def pileup_reweight(events,year):
    weights = coffea.analysis_tools.Weights(len(events),storeIndividual=True)
    
