from coffea.lookup_tools import extractor
from coffea.jetmet_tools import FactorizedJetCorrector, JetCorrectionUncertainty
from coffea.jetmet_tools import JECStack, CorrectedJetsFactory
import awkward as ak
import numpy as np
import os

from DataPath import getDataPath


class JEC():
    def getCorrectionVersion(self,run,isData):
        corr = None
        if '2016' in run:
            if isData:
                runletter = run[4]
                if runletter in 'BCD':
                    corr = 'Summer19UL16APV_RunBCD_V7_DATA'
                if runletter in 'EF' and 'APV' in run:
                    corr = 'Summer19UL16APV_RunEF_V7_DATA/'
                if runletter in 'FGH' and not 'APV' in run:
                    corr = 'Summer19UL16_RunFGH_V7_DATA'
            else:
                corr = 'Summer19UL16_V7_MC'
        if '2017' in run:
            if isData:
                runletter = run[4:]
                corr = 'Summer19UL17_Run%s_V6_DATA'%runletter
            else:
                corr = 'Summer19UL17_V6_MC'
        if '2018' in run:
            if isData:
                runletter = run[4:]
                corr = 'Summer19UL18_Run%s_V5_DATA'%runletter
            else:
                corr = 'Summer19UL18_V5_MC'

        return corr

    def __init__(self,run,isData): #CORRECTION_VERSION,CORRECTION_SET = ['L1FastJet_AK4PFchs','L2Relative_AK4PFchs','L3Absolute_AK4PFchs','L2L3Residual_AK4PFchs']):

        CORRECTION_VERSION = self.getCorrectionVersion(run,isData)
        CORRECTION_SET = ['L1FastJet_AK4PFchs','L2Relative_AK4PFchs','L3Absolute_AK4PFchs','L2L3Residual_AK4PFchs']
        print("Using JEC",CORRECTION_VERSION)
        
        datapath = getDataPath()

        weight_sets = ['* * %s'%(os.path.join(datapath,'JECDatabase/textFiles',CORRECTION_VERSION,CORRECTION_VERSION+'_'+correctionset+'.txt')) for correctionset in CORRECTION_SET]
        jec_stack_names = [CORRECTION_VERSION+'_'+correctionset for correctionset in CORRECTION_SET]

        ext = extractor()
        ext.add_weight_sets(weight_sets)
        ext.finalize()

        evaluator = ext.make_evaluator()

        jec_inputs = {name: evaluator[name] for name in jec_stack_names}
        jec_stack = JECStack(jec_inputs)

        name_map = jec_stack.blank_name_map
        name_map['JetPt'] = 'pt'
        name_map['JetMass'] = 'mass'
        name_map['JetEta'] = 'eta'
        name_map['JetA'] = 'area'
        name_map['ptRaw'] = 'pt_raw'
        name_map['massRaw'] = 'mass_raw'
        name_map['Rho'] = 'rho'

        self.jet_factory = CorrectedJetsFactory(name_map, jec_stack)

    def apply(self,events,inputjets):
        jets = inputjets

        jets['pt_raw'] = (1 - jets['rawFactor']) * jets['pt']
        jets['mass_raw'] = (1 - jets['rawFactor']) * jets['mass']
        jets['rho'] = ak.broadcast_arrays(events.fixedGridRhoFastjetAll, jets.pt)[0]

        events_cache = events.caches[0]

        corrected_jets = self.jet_factory.build(jets, lazy_cache=events_cache)
        return corrected_jets
"""
def JEC(events,jets):

    ###############

#    CORRECTION_VERSION = 'Summer19UL17_RunB_V6_DATA'
#    CORRECTION_SET = ['L1FastJet_AK4PFchs','L2Relative_AK4PFchs','L3Absolute_AK4PFchs','L2L3Residual_AK4PFchs']

    ###############

    from coffea.lookup_tools import extractor
    from coffea.jetmet_tools import FactorizedJetCorrector, JetCorrectionUncertainty
    from coffea.jetmet_tools import JECStack, CorrectedJetsFactory
    import awkward as ak
    import numpy as np
    import os

    from DataPath import getDataPath
    datapath = getDataPath()


    weight_sets = ['* * %s'%(os.path.join(datapath,'JECDatabase',CORRECTION_VERSION,CORRECTION_VERSION+'_'+correctionset+'.txt')) for correctionset in CORRECTION_SET]
    jec_stack_names = [CORRECTION_VERSION+'_'+correctionset for correctionset in CORRECTION_SET]

    ext = extractor()
    ext.add_weight_sets(weight_sets)
    ext.finalize()

    evaluator = ext.make_evaluator()

    jec_inputs = {name: evaluator[name] for name in jec_stack_names}
    jec_stack = JECStack(jec_inputs)

    name_map = jec_stack.blank_name_map
    name_map['JetPt'] = 'pt'
    name_map['JetMass'] = 'mass'
    name_map['JetEta'] = 'eta'
    name_map['JetA'] = 'area'

    jets = events.Jet

    jets['pt_raw'] = (1 - jets['rawFactor']) * jets['pt']
    jets['mass_raw'] = (1 - jets['rawFactor']) * jets['mass']
    jets['rho'] = ak.broadcast_arrays(events.fixedGridRhoFastjetAll, jets.pt)[0]

    name_map['ptRaw'] = 'pt_raw'
    name_map['massRaw'] = 'mass_raw'
    name_map['Rho'] = 'rho'

    events_cache = events.caches[0]

    jet_factory = CorrectedJetsFactory(name_map, jec_stack)
    corrected_jets = jet_factory.build(jets, lazy_cache=events_cache)

    return corrected_jets
"""
